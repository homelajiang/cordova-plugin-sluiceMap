package com.sdr.sluicemapplugin;

import java.io.Serializable;

/**
 * Created by 何永锋 on 2016/8/25.
 */
public class WaterGate implements Serializable{

    /**
     * autoType : 1
     * id : 1
     * lgtd : 120.871651
     * lttd : 30.964581
     * name : 曹坟港闸站
     * status : 1
     * statusTm : 1471403185000
     * type : 1
     * videoCount : 2
     */
    private int autoType;//自动化
    private int id;
    private double lgtd;
    private double lttd;
    private String name;
    private int status;//闸站当前状态
    private long statusTm;//闸站状态变更时间
    private int type;//类型
    private int videoCount;//监控数量

    public int getAutoType() {
        return autoType;
    }

    public void setAutoType(int autoType) {
        this.autoType = autoType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLgtd() {
        return lgtd;
    }

    public void setLgtd(double lgtd) {
        this.lgtd = lgtd;
    }

    public double getLttd() {
        return lttd;
    }

    public void setLttd(double lttd) {
        this.lttd = lttd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getStatusTm() {
        return statusTm;
    }

    public void setStatusTm(long statusTm) {
        this.statusTm = statusTm;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(int videoCount) {
        this.videoCount = videoCount;
    }
}
